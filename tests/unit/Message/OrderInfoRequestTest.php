<?php
/**
 * Created by Rubikin Team.
 * Date: 4/20/14
 * Time: 2:00 AM
 * Question? Come to our website at http://rubikin.com
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Nilead\ShipmentGHNBundle\tests\unit\Message;

use Nilead\ShipmentCommonBundle\tests\unit\TestCase;
use Nilead\ShipmentCommonBundle\tests\unit\TraitPackageTest;
use Nilead\ShipmentGHNBundle\Message\OrderInfoRequest;

class OrderInfoRequestTest extends TestCase
{
    protected $request;

    use TraitPackageTest;

    public function setUp()
    {
        $this->request = new OrderInfoRequest();
        $this->request->setServices($this->getHttpClient(), $this->getHttpRequest());
    }

    public function testGetData()
    {
        $this->request->setClientID(2619)
            ->setSessionToken('')
            ->setPassword('JpTwK4tNkm1VENv8V')
            ->setTransactionReference('7184934551111111111');

        $this->assertEquals([
                "ClientID" => 2619,
                "Password" => "JpTwK4tNkm1VENv8V",
                "SessionToken" => "",
                "GHNOrderCode" => "7184934551111111111"
            ]
            , $this->request->getData());
    }
}
