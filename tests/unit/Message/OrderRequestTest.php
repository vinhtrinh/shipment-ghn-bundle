<?php
/**
 * Created by Rubikin Team.
 * Date: 4/20/14
 * Time: 2:00 AM
 * Question? Come to our website at http://rubikin.com
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Nilead\ShipmentGHNBundle\tests\unit\Message;

use Nilead\ShipmentCommonBundle\tests\unit\TestCase;
use Nilead\ShipmentCommonBundle\tests\unit\TraitPackageTest;
use Nilead\ShipmentGHNBundle\Message\OrderRequest;
use Mockery as m;


class OrderRequestTest extends TestCase
{
    protected $request;

    use TraitPackageTest;

    public function setUp()
    {
        $this->request = new OrderRequest();
        $this->request->setServices($this->getHttpClient(), $this->getHttpRequest());
    }

    public function testGetData()
    {
        $shipment = m::mock('Nilead\ShipmentCommonComponent\Model\ShippablePackageInterface');
        $shipment->shouldReceive('getShippingMethod->getMethodCode')->andReturn(1);

        $shipment->shouldReceive('getShipToAddress')->andReturn(
            $this->generateAddress([
                'lastName' => 'last name 1',
                'firstName' => 'first name 1',
                'phone' => '123456789',
                'address1' => 'address 1',
                'address2' => 'address 2',
            ])
        );

        $shipment->shouldReceive('getShipFromAddress')->andReturn(
            $this->generateAddress([
                'lastName' => 'last name 2',
                'firstName' => 'first name 2',
                'phone' => '123456789',
                'address1' => 'address 2',
                'address2' => 'address 2',
            ])
        );

        $this->request->setClientID(2619)
            ->setSessionToken('Xw3FS04DDGHUKq3/g/+Q+z9FIZjs8DPNAuj3r02fKCaF1p/5vl2ZVYcX4vz96j8XT4v05wK8bH00K18mBm15LK9yx7HIBFy4VlDkh4ZVB3vCqQ1oK0qP8sw08Q9VJCxW3RlDhauEyDIkE0i8=')
            ->setPassword('JpTwK4tNkm1VENv8V')
            ->setWeight(100)
            ->setFromDistrictCode('1A01')
            ->setToDistrictCode('1A06')
            ->setShippingServiceID(1)
            ->setMethodCode(1)
            ->setRecipientId(1)
            ->setOrderId(2)
            ->setStoreId(2)
            ->setPackage($shipment);

        $this->assertEquals([
                "ClientID" => 2619,
                "Password" => "JpTwK4tNkm1VENv8V",
                "RecipientCode" => 1,
                "RecipientName" => "last name 1 first name 1",
                "RecipientPhone" => "123456789",
                "DeliveryAddress" => "address 1 address 2",
                "DeliveryDistrictCode" => "0221",
                "CODAmount" => 0,
                "Note" => NULL,
                "VendorCode" => 2,
                "ShopTransactionCode" => 2,
                "SenderName" => "last name 1 first name 1",
                "SenderPhone" => "123456789",
                "SenderAddress" => "address 2 address 2",
                "PickAddress" => "address 2 address 2",
                "PickDistrictCode" => "0221",
                "ServiceToUse" => 1,
                "SessionToken" => "Xw3FS04DDGHUKq3/g/+Q+z9FIZjs8DPNAuj3r02fKCaF1p/5vl2ZVYcX4vz96j8XT4v05wK8bH00K18mBm15LK9yx7HIBFy4VlDkh4ZVB3vCqQ1oK0qP8sw08Q9VJCxW3RlDhauEyDIkE0i8="
            ]
            , $this->request->getData());
    }
}
