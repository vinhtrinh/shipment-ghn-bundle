<?php

namespace Nilead\ShipmentGHNBundle\DependencyInjection;

use Nilead\ResourceBundle\DependencyInjection\AbstractExtension;


/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class NileadShipmentGHNExtension extends AbstractExtension
{
    /**
     * {@inheritDoc}
     */
    protected $configFiles = array('services', 'form');

    /**
     * {@inheritDoc}
     */
    protected $configure = self::CONFIGURE_LOADER;
}

