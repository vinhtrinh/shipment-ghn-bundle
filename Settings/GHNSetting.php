<?php
/**
 * Created by Rubikin Team.
 * Date: 8/29/13
 * Time: 4:28 PM
 * Question? Come to our website at http://rubikin.com
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Nilead\ShipmentBundle\Carrier\Setting;


use Nilead\SettingBundle\Setting\AbstractSetting;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GHNSetting extends AbstractSetting
{
    public function buildForm(FormBuilderInterface $builder)
    {
        $builder
            ->add('gateway', 'text')
            ->add('clientID', 'text', array(
                    'required' => false,
                    'label' => 'nilead.client_id'
                )
            )
            ->add('password', 'text', array(
                    'required' => false
                )
            );
    }

    public function buildSettings(OptionsResolver $builder)
    {
        $builder
            ->setDefaults(array(
                    'clientID' => '',
                    'password' => '',
                    'gateway' => 'http://103.20.148.187:8064/Soap/GHNService.svc?wsdl',
                )
            )
            ->setRequired(array(
                    'clientID',
                    'password',
                    'gateway',
                )
            );
    }

    public function getName()
    {
        return 'nilead_shipments_carrier_ups';
    }
}
